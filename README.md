# API SPECS

## Authentication
All API must use this authentication

- Header :
    - X-Api-Key : "your secret api key" / "SECRET"(default)

## Create User

Request:
- Method: POST
- Endpoint: `/api/users`
- Header:
    - Content-Type: application/json
    - Accept: application/json
- Body:

```json
{
  "id" : "string, unique",
  "firstname" : "string",
  "lastname" : "string"  
}
```

Response:

```json
{
  "code": 200,
  "status": "OK",
  "data":{        
       "id" : "string, unique",
       "firstname" : "string",
       "lastname" : "string",
       "createdAt" : "timestamp",
       "updateAt" : "timestamp"
  }
}
```

## Get User
Request:
- Method: GET
- Endpoint: `api/users/{iduser}`
- Header:
    - Content-Type: application/json

Response
```json
{
  "code": 200,
  "status": "OK",
  "data":{        
       "id" : "string, unique",
       "firstname" : "string",
       "lastname" : "string",
       "createdAt" : "timestamp",
       "updateAt" : "timestamp"
  }
}
```

## Update User
Request:
- Method: PUT
- Endpoint: `/api/users/{iduser}`
- Header:
    - Content-Type: application/json
    - Accept: application/json
- Body:

```json
{  
  "firstname" : "string",
  "lastname" : "string"  
}
```

Response:

```json
{
  "code": 200,
  "status": "OK",
  "data":{        
       "id" : "string, unique",
       "firstname" : "string",
       "lastname" : "string",
       "createdAt" : "timestamp",
       "updateAt" : "timestamp"
  }
}
```

## List User
Request:
- Method: GET
- Endpoint: `/api/users`
- Header:    
    - Accept: application/json
- Query Param :
    - size: number,
    - page: number
    
Response:

```json
{
  "code": 200,
  "status": "OK",
  "data":[
      {        
           "id" : "string, unique",
           "firstname" : "string",
           "lastname" : "string",
           "createdAt" : "timestamp",
           "updateAt" : "timestamp"  
      },
      {        
           "id" : "string, unique",
           "firstname" : "string",
           "lastname" : "string",
           "createdAt" : "timestamp",
           "updateAt" : "timestamp" 
      }
  ]
}
```