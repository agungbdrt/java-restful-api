package com.agungbdrt.store.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UnAuthorizeException extends Exception{
}
