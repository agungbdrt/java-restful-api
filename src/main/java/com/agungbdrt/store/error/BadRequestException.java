package com.agungbdrt.store.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BadRequestException extends Exception{
}
