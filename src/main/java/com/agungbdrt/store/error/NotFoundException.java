package com.agungbdrt.store.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotFoundException extends Exception {

}
