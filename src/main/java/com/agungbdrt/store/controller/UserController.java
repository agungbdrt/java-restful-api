package com.agungbdrt.store.controller;

import com.agungbdrt.store.error.BadRequestException;
import com.agungbdrt.store.error.NotFoundException;
import com.agungbdrt.store.model.request.ListUserRequest;
import com.agungbdrt.store.model.request.CreateUserRequest;
import com.agungbdrt.store.model.request.UpdateUserRequest;
import com.agungbdrt.store.model.response.UserResponse;
import com.agungbdrt.store.model.response.WebResponse;
import com.agungbdrt.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(
            value = "/api/users",
            produces = "application/json",
            consumes = "application/json"
    )
    WebResponse<UserResponse> createUser(@RequestBody CreateUserRequest body){

            UserResponse userResponse = userService.create(body);
            return responseToResponse(userResponse);
    }

    @GetMapping(
            value = "/api/users/{iduser}",
            produces = "application/json"
    )
    WebResponse<UserResponse> getUser(@PathVariable(value = "iduser") String iduser) throws NotFoundException {
        UserResponse userResponse = userService.get(iduser);
        return  responseToResponse(userResponse);
    }

    @GetMapping(
            value = "/api/users",
            produces = "application/json"
    )
    WebResponse<List<UserResponse>> listUser(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ) throws BadRequestException {

            ListUserRequest listUserRequest = new ListUserRequest(page,size);
            List<UserResponse> userResponses = userService.list(listUserRequest);
            return responseToResponse(userResponses);

    }

    @PutMapping(
            value = "/api/users/{iduser}",
            produces = "application/json"
    )
    WebResponse<UserResponse> updateUser(@PathVariable(value = "iduser") String id, @RequestBody UpdateUserRequest body) throws NotFoundException {

            UserResponse userResponse = userService.update(id,body);
            return responseToResponse(userResponse);

    }

    private WebResponse<UserResponse> responseToResponse(UserResponse userResponse){
        return new WebResponse<>(
                HttpStatus.OK.value(),
                HttpStatus.OK.name(),
                userResponse
        );
    }

    private WebResponse<List<UserResponse>> responseToResponse(List<UserResponse> userResponse){
        return new WebResponse<>(
                HttpStatus.OK.value(),
                HttpStatus.OK.name(),
                userResponse
        );
    }
    /*@GetMapping(
            value = "/api/users",
            produces = "application/json"
    )
    WebResponse<List<UserResponse>> getUser(){
        try {
            List<UserResponse> userResponse = userService.getAll();
            return new WebResponse<>(
                    200,
                    HttpStatus.OK.toString(),
                    userResponse
            );
        }catch (Exception e){
            return new WebResponse<>(
                    400,
                    HttpStatus.BAD_REQUEST.toString()+" "+e.getMessage(),
                    null
            );
        }
    }*/
}
