package com.agungbdrt.store.controller;

import com.agungbdrt.store.error.BadRequestException;
import com.agungbdrt.store.error.NotFoundException;
import com.agungbdrt.store.error.UnAuthorizeException;
import com.agungbdrt.store.model.response.WebResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler(value = ConstraintViolationException.class)
    WebResponse<String> validationHandler(ConstraintViolationException constraintViolationException){
        return new WebResponse<>(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.name(),
                constraintViolationException.getMessage()
        );
    }

    @ExceptionHandler(value = NotFoundException.class)
    WebResponse<String> notFoundHandler(NotFoundException notFoundException){
        return new WebResponse<>(
                HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND.name(),
                "Not Found"
        );
    }

    @ExceptionHandler(value = UnAuthorizeException.class)
    WebResponse<String> unAuthorizeExceptionHandler(UnAuthorizeException unAuthorizeException){
        return new WebResponse<>(
                HttpStatus.UNAUTHORIZED.value(),
                HttpStatus.UNAUTHORIZED.name(),
                "Please put your api key"
        );
    }

    @ExceptionHandler(value = BadRequestException.class)
    WebResponse<String> unAuthorizeExceptionHandler(BadRequestException badRequestException){
        return new WebResponse<>(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.name(),
                badRequestException.getMessage()
        );
    }

}
