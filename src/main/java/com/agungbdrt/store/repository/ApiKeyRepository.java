package com.agungbdrt.store.repository;

import com.agungbdrt.store.model.entity.ApiKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApiKeyRepository extends JpaRepository<ApiKey,String> {
}
