package com.agungbdrt.store.service.impl;

import com.agungbdrt.store.error.BadRequestException;
import com.agungbdrt.store.error.NotFoundException;
import com.agungbdrt.store.model.request.ListUserRequest;
import com.agungbdrt.store.model.request.CreateUserRequest;
import com.agungbdrt.store.model.entity.User;
import com.agungbdrt.store.model.request.UpdateUserRequest;
import com.agungbdrt.store.model.response.UserResponse;
import com.agungbdrt.store.repository.UserRepository;
import com.agungbdrt.store.service.UserService;
import com.agungbdrt.store.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    ValidationUtil validationUtil;
    @Autowired
    UserRepository userRepository;

    @Override
    public UserResponse create(CreateUserRequest createUserRequest) {
        validationUtil.validate(createUserRequest);
        User user = new User(
                createUserRequest.getId(),
                createUserRequest.getFirstname(),
                createUserRequest.getLastname()
        );
        userRepository.save(user);

        return userToUserResponse(user);
    }

    @Override
    public List<UserResponse> getAll() {
        List<User> userList = userRepository.findAll();
        return userList.stream().map(this::userToUserResponse).collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> list(ListUserRequest listUserRequest) throws BadRequestException {
        validationUtil.validate(listUserRequest);
        if(listUserRequest.getSize() < 1 || listUserRequest.getPage() < 0){
            throw new BadRequestException();
        }

        Page<User> page = userRepository.findAll(PageRequest.of(listUserRequest.getPage(), listUserRequest.getSize()));
        List<User> userList = page.get().collect(Collectors.toList());

        return userList.stream().map(this::userToUserResponse).collect(Collectors.toList());
    }

    @Override
    public UserResponse update(String id, UpdateUserRequest updateUserRequest) throws NotFoundException {
        validationUtil.validate(updateUserRequest);
        User user = new User();
        if(!userRepository.findById(id).isPresent()){
            throw new NotFoundException();
        }
        user = userRepository.findById(id).get();
        user.setFirstname(updateUserRequest.getFirstname());
        user.setLastname(updateUserRequest.getLastname());

        userRepository.save(user);

        return userToUserResponse(user);
    }

    @Override
    public UserResponse get(String id) throws NotFoundException {
        if(userRepository.findById(id).isPresent()){
            User user = userRepository.findById(id).get();
            return userToUserResponse(user);
        }else{
            throw new NotFoundException();
        }
    }

    private UserResponse userToUserResponse(User user){
        return new UserResponse(
                user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getCreatedAt(),
                user.getUpdateAt()
        );
    }
}
