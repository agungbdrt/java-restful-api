package com.agungbdrt.store.service;

import com.agungbdrt.store.error.BadRequestException;
import com.agungbdrt.store.error.NotFoundException;
import com.agungbdrt.store.model.request.ListUserRequest;
import com.agungbdrt.store.model.request.CreateUserRequest;
import com.agungbdrt.store.model.request.UpdateUserRequest;
import com.agungbdrt.store.model.response.UserResponse;

import java.util.List;

public interface UserService {
    UserResponse create(CreateUserRequest createUserRequest);
    List<UserResponse> getAll();
    List<UserResponse> list(ListUserRequest listUserRequest) throws BadRequestException;
    UserResponse update(String id, UpdateUserRequest updateUserRequest) throws NotFoundException;
    UserResponse get(String id) throws NotFoundException;
}
