package com.agungbdrt.store.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    private String id;

    private String firstname;

    private String lastname;

    private Date createdAt;

    private Date updateAt;
}
