package com.agungbdrt.store.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = ApiKey.TABLE_NAME)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiKey extends BaseEntity{
    public static final String TABLE_NAME = "tb_m_api_key";

    @Id
    private String id;
}
