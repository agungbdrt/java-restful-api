package com.agungbdrt.store.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListUserRequest {
    @Min(value = 0)
    int page;

    @Min(value = 1)
    int size;
}
