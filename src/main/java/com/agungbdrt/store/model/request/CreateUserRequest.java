package com.agungbdrt.store.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateUserRequest {
    private String id;

    @NotBlank
    private String firstname;

    @NotBlank
    private String lastname;
}
