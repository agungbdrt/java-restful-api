package com.agungbdrt.store.auth;

import com.agungbdrt.store.error.UnAuthorizeException;
import com.agungbdrt.store.repository.ApiKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

@Component
public class ApiKeyInterceptor implements WebRequestInterceptor {

    @Autowired
    ApiKeyRepository apiKeyRepository;

    @Override
    public void preHandle(WebRequest webRequest) throws Exception {
        String apikey = webRequest.getHeader("X-Api-Key");
        if(apikey == null){
            throw new UnAuthorizeException();
        }

        if (!apiKeyRepository.existsById(apikey)){
            throw new UnAuthorizeException();
        }
        //valid
    }

    @Override
    public void postHandle(WebRequest webRequest, ModelMap modelMap) throws Exception {
        //nothing
    }

    @Override
    public void afterCompletion(WebRequest webRequest, Exception e) throws Exception {
        //nothing
    }
}
