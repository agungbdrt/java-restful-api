package com.agungbdrt.store.config;

import com.agungbdrt.store.model.entity.ApiKey;
import com.agungbdrt.store.repository.ApiKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ApiKeySeeder implements ApplicationRunner {
    @Autowired
    ApiKeyRepository apiKeyRepository;

    final String apiKey = "SECRET";

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(!apiKeyRepository.existsById(apiKey)){
            ApiKey key = new ApiKey(apiKey);
            apiKeyRepository.save(key);
        }
    }
}
